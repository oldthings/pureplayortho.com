/************************************************************* 
 
  ClinicianSites Scripts 
  Copyright 2010, All Rights Reserved 
  By HelloMedical //////// 
 
*************************************************************/ 
 
/************************************** 
  iFrame Popup 
**************************************/ 
  $(document).ready(function() { 
     
    var browserheight = jQuery(document).height(); 
    var percentage = 50; 
    var fbheight = Math.ceil((browserheight*percentage)/100); 
 
    $("a.module").fancybox({       
      'width'        : 500, 
      'height'      : fbheight, 
      'type'        : 'iframe' 
    }); 
 
  });