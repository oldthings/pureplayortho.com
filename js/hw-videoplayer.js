function makeVideoPlayer( jsonURL, videoContainer, chapterContainer, chapterInfo ){ 
   
  loadJSON( jsonURL, function(data){ 
     
    // build video player 
     
    var vidID = videoContainer.attr("id"); 
     
    $f(vidID, {src: "http://www.hellomedical.com/swf/flowplayer.swf"}, { 
      canvas:  { 
        background: '#000000', 
        gradient:'none' 
      }, 
      clip: { 
        url: data.url, 
        autoPlay: false, 
        autoBuffering: true, 
          scaling: "fit" 
      }, 
      plugins:{ 
        controls:{ 
          height:20, 
          backgroundColor: '#000000', 
          backgroundGradient: 'none' 
        } 
      }, 
      play: { 
        label: null, 
        replayLabel: "click to replay" 
      } 
    }); 
     
    // build chapter list     
    if ( data.chapters ) { 
       
      var chapterList; 
      var chapterListA = $("<ol id='chapterListA'></ol>"); 
      chapterContainer.append(chapterListA); 
      var chapterListB = $("<ol id='chapterListB'></ol>"); 
      chapterContainer.append(chapterListB); 
       
      chapterList = chapterListA; 
       
      for (var i=0; i < data.chapters.length; i++) { 
         
        if ( i == Math.floor(data.chapters.length/2) ) { 
          chapterList = chapterListB; 
          chapterList.attr("start",i+1); 
        }; 
         
        var chapter = data.chapters[i]; 
         
        var timeMins = (Math.floor(chapter.timecode/60)).toString(); 
        var timeSecs = (chapter.timecode%60).toString(); 
         
        if ( timeMins.length == 1 ) { timeMins = "0" + timeMins }; 
        if ( timeSecs.length == 1 ) { timeSecs = "0" + timeSecs }; 
         
        var chapterButton = $('<a href="#t='+chapter.timecode+'"><em>('+timeMins+':'+timeSecs+')</em> '+chapter.title+'</a>'); 
         
        chapterButton.attr( "timecode", chapter.timecode ); 
        chapterButton.data( "info", chapter.info ); 
         
        chapterList.append( $("<li></li>").append( chapterButton ) ); 
         
      };   
             
      var cueList = []; 
 
      chapterContainer.find("a").each(function(i){ 
 
        var timecode = parseInt( $(this).attr("href").replace( "#", "" ).replace( "t=", "" ) );           
        cueList.push( timecode*1000 ); 
 
        $(this).click(function(e){ 
          e.preventDefault(); 
 
          document.location.hash = "t=" + $(this).attr( "timecode" ); 
 
          chapterContainer.find("a.selected").removeClass('selected'); 
          $(this).addClass('selected'); 
           
          $f(vidID).seek( parseInt( $(this).attr( "timecode" ) ) ); 
          if ( !$f(vidID).isPlaying() ) { 
            $f(vidID).play(); 
          }; 
           
          var button = $(this); 
          chapterInfo.html( button.data("info") ); 
           
        }); 
 
      }); 
 
      $f(vidID).onCuepoint( cueList, function(clip, cuepoint){ 
        chapterContainer.find("a.selected").removeClass('selected'); 
        chapterContainer.find("a[timecode='"+cuepoint/1000+"']").addClass('selected'); 
         
        document.location.hash = "t=" + cuepoint/1000; 
         
        var button = chapterContainer.find("a.selected"); 
        chapterInfo.html( button.data("info") ); 
         
      }); 
       
    }; 
     
  } ); 
   
} 
 
function loadJSON ( jsonURL, handler ){ 
  try { $.get( jsonURL, {}, handler, "json" ); 
  } catch(e){ 
    alert(e); 
  }   
} 
