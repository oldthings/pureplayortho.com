/*   ------------------------------------------------- 
 
HELLOMEDICAL FORM TOOLS 
(c) 2011 HELLOMEDICAL 
written by Isaac Williams 
 
-----------------------------------------------*/ 
 
(function($){ 
   
  /*   ------------------------------------------------- 
    defaultInput 
 
    display a hint to users in textfields 
 
    @PARARMS: 
        - message: message to be displayed before user selects 
          defaults to inputs value attribute 
        - color: color of hint text   
 
  ---------------------------------------------------*/ 
   
  $.fn.defaultInput = function( options ) { 
    var settings = { 
      message:"", 
      color:"#999999" 
    };       
    return this.each(function() { 
      if ( options ) { $.extend( settings, options ); }; 
         
      var obj = $(this); 
       
      var message = settings.message;       
      if ( settings.message == "" && obj.attr("value") != "" ) { 
        message = obj.attr("value"); 
      }; 
       
      var resetColor = obj.css( 'color' ); 
      obj.css( 'color', settings.color ); 
      obj.val( message ); 
             
      obj.focusin(function(e){ 
        if ( obj.val() == message ) { 
          obj.css( 'color', resetColor ); 
          obj.val( "" ); 
        }; 
      });     
      obj.blur(function(e){ 
        if ( obj.val() == "" ) { 
          obj.val( message ); 
          obj.css( 'color', settings.color ); 
        } 
      }); 
      obj.keyup( function(e){ 
        if ( obj.val() != "" ) { 
          obj.data( 'edited', true ); 
          obj.unbind( 'focusin' ); 
          obj.unbind( 'blur' ); 
        } 
      }); 
       
      }); 
  }; 
   
  /*   ------------------------------------------------- 
     
    makeLabelInline 
 
  ---------------------------------------------------*/ 
   
  $.fn.makeLabelInline = function( options ) { 
    var defaults = { 
     
    }; 
   
    var options = $.extend( defaults, options ); 
     
    return this.each(function() {   
        var obj = $(this); 
       
      var labelText = obj.html();       
      var target = $("#"+obj.attr("for")); 
       
      target.defaultInput( { message:labelText } ); 
       
      obj.remove(); 
       
    }); 
     
  } 
   
  /*   ------------------------------------------------- 
    makeEditable 
 
    makes a div an editable area on doubleclick 
 
  ---------------------------------------------------*/ 
   
  $.fn.makeEditable = function( options ) {   
     
    var defaults = { 
     
    }; 
   
    var options = $.extend( defaults, options ); 
   
      return this.each(function() {   
        var obj = $(this); 
     
      var wrapText = function(textArea, openTag, closeTag) { 
          var len = textArea.val().length; 
          var start = textArea[0].selectionStart; 
          var end = textArea[0].selectionEnd; 
          var selectedText = textArea.val().substring(start, end); 
          var replacement = openTag + selectedText + closeTag; 
          textArea.val(textArea.val().substring(0, start) + replacement + textArea.val().substring(end, len)); 
      } 
     
      var edit = function(e){   
        var htmlContents = obj.html();       
        htmlContents = $.trim(htmlContents); 
        obj.html( "<div class='tools'></div><textarea></textarea>" ); 
       
        obj.children('.tools').append( "<a href='#' id='bold'>[bold]</a>" ); 
        obj.children('.tools').append( "<a href='#' id='italic'>[italic]</a>" ); 
       
        obj.children('.tools').append( "<a href='#' id='done'>&#10004; done</a>" ); 
       
        var editarea = obj.find("textarea"); 
             
        $.get( 'markdown/html-to-md.php', {"text":htmlContents}, function(data){         
          editarea.val( data );         
        } ); 
       
        obj.find('.tools a#bold').click(function(e){ 
          wrapText( editarea, "**", "**" ); 
        }); 
        obj.find('.tools a#italic').click(function(e){ 
          wrapText( editarea, "*", "*" );         
        }); 
       
        obj.find('.tools a#done').click(function(e){ 
          e.preventDefault(); 
           
          var sendText = editarea.val(); 
           
          $.get( 'markdown/md-to-html.php', {"text":sendText}, function(data){ 
            obj.html( data );   
            obj.one( "dblclick", edit );         
          }); 
               
        }); 
      } 
     
      $(this).one( "dblclick", edit ); 
     
      }); 
   }; 
   
   
})(jQuery); 
